package dam.androidsanti.u4t6contacts;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    private MyContacts myContacts;

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textView;

        public MyViewHolder(TextView view){
            super(view);
            this.textView = view;
        }

        public void bind(String contactData) {
            this.textView.setText(contactData);
        }
    }

    MyAdapter(MyContacts myContacts){
        this.myContacts = myContacts;
    }

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        TextView tv = (TextView) LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);

        return new MyViewHolder(tv);
    }

    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position){

        viewHolder.bind(myContacts.getContactData(position));
    }

    public  int getItemCount(){
        return myContacts.getCount();
    }
}
