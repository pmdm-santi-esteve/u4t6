package dam.androidsanti.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {

    private ArrayList<ContactItem> myDataSet;
    private Context context;

    public MyContacts(Context context){

        this.context = context;
        this.myDataSet = getContacts();
    }

    //TODO: Ex1 - Modificamos la consulta para que se seleccionen los datos pedidos
    public ArrayList<ContactItem> getContacts(){

        ArrayList<ContactItem> contactList = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();

        String[] projection = new String[]{ContactsContract.Data._ID,
        ContactsContract.Data.DISPLAY_NAME,
        ContactsContract.CommonDataKinds.Phone.NUMBER,
        ContactsContract.Data.CONTACT_ID,
        ContactsContract.Data.LOOKUP_KEY,
        ContactsContract.Data.RAW_CONTACT_ID,
        ContactsContract.CommonDataKinds.Phone.TYPE,
        ContactsContract.Data.PHOTO_THUMBNAIL_URI};

        String selectionFilter = ContactsContract.Data.MIMETYPE+"='"+
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE+"' AND "+
                ContactsContract.CommonDataKinds.Phone.NUMBER+" IS NOT NULL";

        Cursor contactsCursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, projection,
                selectionFilter, null, ContactsContract.Data.DISPLAY_NAME+" ASC");

        if(contactsCursor != null){

            int idIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int nameIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex = contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int contactIDIndex = contactsCursor.getColumnIndexOrThrow((ContactsContract.Data.CONTACT_ID));
            int lookupKeyIndex = contactsCursor.getColumnIndexOrThrow((ContactsContract.Data.LOOKUP_KEY));
            int rawContactIdIndex = contactsCursor.getColumnIndexOrThrow((ContactsContract.Data.RAW_CONTACT_ID));
            int phoneTypeIndex = contactsCursor.getColumnIndexOrThrow((ContactsContract.CommonDataKinds.Phone.TYPE));
            int photoThumbIndex = contactsCursor.getColumnIndexOrThrow((ContactsContract.Data.PHOTO_THUMBNAIL_URI));

            while(contactsCursor.moveToNext()){

                String id = contactsCursor.getString(idIndex);
                String name = contactsCursor.getString(nameIndex);
                String number = contactsCursor.getString(numberIndex);
                String contactId = contactsCursor.getString(contactIDIndex);
                String lookupKey = contactsCursor.getString(lookupKeyIndex);
                String rawContact = contactsCursor.getString(rawContactIdIndex);
                int phoneType = Integer.parseInt(contactsCursor.getString(phoneTypeIndex));
                String photourl = contactsCursor.getString(photoThumbIndex);

                contactList.add(new ContactItem(id, number, name, contactId, lookupKey, rawContact, phoneType, photourl));
            }
            contactsCursor.close();
        }

        return contactList;
    }

    public ContactItem getContactData(int position){
        return myDataSet.get(position);
    }

    public int getCount(){
        return myDataSet.size();
    }
}
