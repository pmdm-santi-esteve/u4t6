package dam.androidsanti.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener{
    private MyContacts m;
    MyContacts myContacts;
    RecyclerView recyclerView;
    TextView tv;

    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if(checkPermissions()){
            setListAdapter();
        }

    }

    private void setUI(){

        tv = findViewById(R.id.textView);
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        //TODO Ex2 - Al movernos por los contactos, se ocultara el text view
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                tv.setText("");
            }
        });

    }

    private void setListAdapter(){

        myContacts = new MyContacts(this);

        recyclerView.setAdapter(new MyAdapter(myContacts.getContacts(), this));

        if (myContacts.getCount() > 0) findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
    }

    private boolean checkPermissions(){

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;
        } else{
            return true;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CONTACTS){

            if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                setListAdapter();
            else
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();

        }else
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //TODO EX2 - Mostramos en el textView los datos del contacto
    @Override
    public void onItemClick(ContactItem item) {

        tv.setText(item.getDisplay_name()+" "+item.getNumber()+" "+item.getPhone_type()+"\n _ID: "+item.getId()+" CONTACT_ID: "+item.getContact_id()+
                " RAW_CONTACT_ID: "+item.getRaw_contact_id()+"\n LOOKUP_KEY: "+item.getLookup_key());
    }
}
