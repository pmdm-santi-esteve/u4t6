package dam.androidsanti.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    public interface OnItemClickListener{

        void onItemClick(ContactItem item);

    }

    private ArrayList<ContactItem> myDataSet;
    private OnItemClickListener listener;

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView id;
        TextView nombre;
        TextView numero;
        ImageView imagen;
        ConstraintLayout cl;

        public MyViewHolder(View view){

            super(view);
            imagen = view.findViewById(R.id.imagen);
            id = view.findViewById(R.id.contactId);
            nombre = view.findViewById(R.id.nombre);
            numero = view.findViewById(R.id.numero);
            cl = view.findViewById(R.id.constItem);
        }

        public void bind(ContactItem item, final OnItemClickListener listener){
            this.id.setText(item.getId());
            this.nombre.setText(item.getDisplay_name());
            this.numero.setText(item.getNumber());

            if(item.getPhkoto_thumnail_url() != null){

                this.imagen.setImageURI(Uri.parse(item.getPhkoto_thumnail_url()));
            }else{

                this.imagen.setImageResource(R.mipmap.ic_launcher);
            }

            this.cl.setOnClickListener(v -> listener.onItemClick(item));
        }
    }

    MyAdapter(ArrayList<ContactItem> myDataSet, OnItemClickListener listener){
        this.myDataSet = myDataSet;
        this.listener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){

        View tv = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemview,parent, false);

        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position){

        viewHolder.bind(myDataSet.get(position), listener);
    }

    @Override
    public int getItemCount(){

        return myDataSet.size();
    }
}
