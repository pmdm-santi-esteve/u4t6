package dam.androidsanti.u4t6contacts;

import static android.provider.ContactsContract.CommonDataKinds.Phone.TYPE_HOME;
import static android.provider.ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE;
import static android.provider.ContactsContract.CommonDataKinds.Phone.TYPE_WORK;

public class ContactItem {

    private String id;
    private String number;
    private String display_name;
    private String contact_id;
    private String lookup_key;
    private String raw_contact_id;
    private String phone_type;
    private String phkoto_thumnail_url;

    public ContactItem(String id, String number, String display_name, String contact_id, String lookup_key,
                       String raw_contact_id, int phone_type, String phkoto_thumnail_url) {
        this.id = id;
        this.number = number;
        this.display_name = display_name;
        this.contact_id = contact_id;
        this.lookup_key = lookup_key;
        this.raw_contact_id = raw_contact_id;

        //TODO Ex2 - Diferenciamos los distintos tipos de numeros de telefono que nos pide el ejercicio
        if(phone_type == TYPE_HOME){
            this.phone_type = "HOME";
        }
        else if(phone_type == TYPE_MOBILE){
            this.phone_type = "MOBILE";
        }
        else if(phone_type == TYPE_WORK){
            this.phone_type = "WORK";
        }
        else{
            this.phone_type = "OTHER";
        }

        this.phkoto_thumnail_url = phkoto_thumnail_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getLookup_key() {
        return lookup_key;
    }

    public void setLookup_key(String lookup_key) {
        this.lookup_key = lookup_key;
    }

    public String getRaw_contact_id() {
        return raw_contact_id;
    }

    public void setRaw_contact_id(String raw_contact_id) {
        this.raw_contact_id = raw_contact_id;
    }

    public String getPhone_type() {
        return phone_type;
    }

    public void setPhone_type(String phone_type) {
        this.phone_type = phone_type;
    }

    public String getPhkoto_thumnail_url() {
        return phkoto_thumnail_url;
    }

    public void setPhkoto_thumnail_url(String phkoto_thumnail_url) {
        this.phkoto_thumnail_url = phkoto_thumnail_url;
    }
}
